package com.kpstv.xclipper.data.provider

import android.content.Context
import android.os.Build
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.kpstv.hvlog.HVLog
import com.kpstv.license.Encryption.Decrypt
import com.kpstv.xclipper.App.APP_MAX_DEVICE
import com.kpstv.xclipper.App.APP_MAX_ITEM
import com.kpstv.xclipper.App.DeviceID
import com.kpstv.xclipper.App.UID
import com.kpstv.xclipper.App.bindDelete
import com.kpstv.xclipper.App.bindToFirebase
import com.kpstv.xclipper.App.getMaxConnection
import com.kpstv.xclipper.App.getMaxStorage
import com.kpstv.xclipper.App.gson
import com.kpstv.xclipper.data.localized.FBOptions
import com.kpstv.xclipper.data.localized.dao.UserEntityDao
import com.kpstv.xclipper.data.model.*
import com.kpstv.xclipper.extensions.*
import com.kpstv.xclipper.extensions.listeners.*
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.*
import javax.inject.Inject

class FirebaseProviderImpl @Inject constructor(
    @ApplicationContext private val context: Context,
    private val dbConnectionProvider: DBConnectionProvider,
    private val currentUserRepository: UserEntityDao
) : FirebaseProvider {

    private val TAG = javaClass.simpleName

    private var isInitialized = MutableLiveData(false)

    private var validDevice: Boolean = false
    private var licenseStrategy = MutableLiveData(LicenseType.Invalid)

    override fun isInitialized() = isInitialized

    override fun initialize(options: FBOptions?, notifyInitialization: Boolean) {
        HVLog.d()
        Log.e(TAG, "Firebase Database has been initialized")
    }

    override fun uninitialized() {
    }

    override suspend fun isLicensed(): Boolean = currentUserRepository.isLicensed() ?: false

    override fun isValidDevice(): Boolean = validDevice

    override fun getLicenseStrategy() = licenseStrategy

    override suspend fun clearData() {
        currentUserRepository.remove()
    }

    override suspend fun addDevice(DeviceId: String): ResponseResult<Unit> {
        return ResponseResult.complete(Unit)
    }


    override suspend fun removeDevice(DeviceId: String): ResponseResult<Unit> {
        return ResponseResult.complete(Unit)
    }

    override suspend fun replaceData(unencryptedOldClip: Clip, unencryptedNewClip: Clip) {
    }

    override suspend fun deleteMultipleData(unencryptedClips: List<Clip>) {
    }

    override suspend fun deleteData(unencryptedClip: Clip) {
    }

    override suspend fun uploadData(unencryptedClip: Clip) {
    }

    override suspend fun getAllClipData(): List<Clip>? {
        return null
    }

    override fun isObservingChanges() = job?.isActive ?: false

    private var job: CompletableJob? = null
    override fun observeDataChange(
        changed: (List<Clip>) -> Unit,
        removed: (List<String>?) -> Unit,
        removedAll: SimpleFunction,
        error: (Exception) -> Unit,
        deviceValidated: (Boolean) -> Unit,
        inconsistentData: SimpleFunction
    ) {}

    override fun removeDataObservation() {
    }
}